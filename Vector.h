#include <iostream>
#include <cmath> 

class Vector
{
public:
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}

	void printVector();
	double getModuleVector();

private:
	double x = 0;
	double y = 0;
	double z = 0;
};

