#include "Vector.h"

void Vector::printVector()
{
	std::cout << "x: " << x << + " y: " << y << " z: " << z << std::endl;
}

double Vector::getModuleVector()
{
	return std::sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
}